FROM alpine:3.8

MAINTAINER Hristo Hristov hristo.dr.hristov@gmail.com

RUN apk update && apk upgrade \
      && apk add pdftk
